import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configSharedPreferences.dart';
import 'package:movil_nanny/src/Services/chat_socket.dart';
import 'package:movil_nanny/src/Services/provider.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/routes.dart';
import 'package:movil_nanny/src/screens/cuestionario/cuest_screen.dart';
import 'package:movil_nanny/src/screens/home/home_screen.dart';
import 'package:movil_nanny/src/screens/home/pendiente.dart';
import 'package:movil_nanny/src/screens/sign_in/sign_in_screen.dart';
import 'package:movil_nanny/src/theme.dart';
import 'package:provider/provider.dart';
import './src/Config/config.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ChatService()),
        ChangeNotifierProvider(create: (_) => ProviderInfor())
      ],
      child: MaterialApp(
        navigatorKey: navigatorKey,
        color: kPrimaryColor,
        title: 'Nannys',
        debugShowCheckedModeBanner: false,
        home: FutureBuilder(
          future: obtenerSesion(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done && snapshot.data != null) {
              var val = json.decode(snapshot.data);
              if (val['ok'] == peticionCorrecta) {
                return HomeScreen();
              }
              if (val['ok'] == sinCuestionario ) {
                return CuestionarioScreen();
              }
              if (val['ok'] == pendiente ) {
                return PendienteScreen();
              }
            } else {
              return SignInScreen();
            }
            return Container();
          },
        ),
        theme: theme(),
        routes: routes,
      ),
    );
  }
}
