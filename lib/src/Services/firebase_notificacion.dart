import 'dart:async';
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:movil_nanny/src/screens/profile/profile_screen.dart';
import 'package:movil_nanny/src/screens/solicitudes_servicio/solicitud_screen.dart';
import '../Config/config.dart';
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
List person;


class Notificacion {
  final streams = StreamController<dynamic>.broadcast();
  Stream<dynamic> get mensaje => streams.stream;
  var argumento;
  final localNotification = LocalNotification();
  navegar(msg) {
    argumento = msg;
    streams.sink.add(argumento);
  }
  
  //token para reconocer cada dispositivo
  getTokenFmc() async {
    _firebaseMessaging.requestNotificationPermissions();
    var token = await _firebaseMessaging.getToken();
    return token;
  }

  // escuchar cuando llega una notificacion
  initNotifications(context) async {
    _firebaseMessaging.requestNotificationPermissions();
    localNotification.init();
    _firebaseMessaging.configure(
      //dentro de la aplicacionr
      
      onMessage: (msg) async {
        Map notificacion = {
          'titulo': msg['notification']['title'],
          'mensaje':msg['notification']['body'],
          'data':msg['data']
        };
        localNotification.mostrarNotificacion(notificacion);
        if(msg['data']['pantalla'] == 'cuenta')
          return navigatorKey.currentState.pushNamed(ProfileScreen.routeName);
        
        if(msg['data']['pantalla'] == 'solicitud')
          return navigatorKey.currentState.pushNamed(SolicitudesScreen.routeName);
        
        navegar(msg['data']);
      },
      onLaunch: (msg) async {
        Map notificacion = {
          'titulo': msg['notification']['title'],
          'mensaje':msg['notification']['body'],
          'data':msg['data']
        };
        localNotification.mostrarNotificacion(notificacion);
        if(msg['data']['pantalla'] == 'cuenta')
          return navigatorKey.currentState.pushNamed(ProfileScreen.routeName);
        
        if(msg['data']['pantalla'] == 'solicitud')
          return navigatorKey.currentState.pushNamed(SolicitudesScreen.routeName);
        navegar(msg);
      },
      onResume: (msg) async {
        Map notificacion = {
          'titulo': msg['notification']['title'],
          'mensaje':msg['notification']['body'],
          'data':msg['data']
        };
        localNotification.mostrarNotificacion(notificacion);
        if(msg['data']['pantalla'] == 'cuenta')
          return navigatorKey.currentState.pushNamed(ProfileScreen.routeName);
        
        if(msg['data']['pantalla'] == 'solicitud')
          return navigatorKey.currentState.pushNamed(SolicitudesScreen.routeName);
        navegar(msg);
      },
    );
  }
  // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project

  dispose() {
    streams.close();
  }
}

class LocalNotification {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('ic_launcher');
  
  init() async {
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid,iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,onSelectNotification: selectNotification);
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      var datos = json.decode(payload);
      if(datos['pantalla'] == 'cuenta'){
        return navigatorKey.currentState.pushNamed(ProfileScreen.routeName);
      }
      if(datos['pantalla'] == 'solicitud')
        return navigatorKey.currentState.pushNamed(SolicitudesScreen.routeName);
    }
  }

  Future onDidReceiveLocalNotification(int i, String a, String b, String c) async {}

  mostrarNotificacion(Map data) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'your channel id', 'your channel name', 'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, data['titulo'], data['mensaje'], platformChannelSpecifics,
        payload: json.encode(data['data']));
  }
}
