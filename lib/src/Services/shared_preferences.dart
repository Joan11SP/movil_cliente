import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

String session = 'perfil';

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

guardarPerfil(sesion) async {
  final shared = await _prefs;
  shared.setString(session, json.encode(sesion));
}

obtenerPerfil() async {
  final shared = await _prefs;
  return shared.getString(session);
}
