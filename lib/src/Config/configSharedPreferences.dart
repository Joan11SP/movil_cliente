//import 'dart:convert';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

String session = 'session';
String emailValidar = 'validandoEmial';
String cambiarClave = 'cambiarclave';
String codigo =  'codigo-otp';
String fmc = '';
String perfilChat='perfil-chat';
Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

obtenerSesion() async {
  final shared = await _prefs;
  return shared.getString(session);
}

guardarLogin(Map login) async {
  final shared = await _prefs;
  var guardar = shared.setString(session, json.encode(login));
  return guardar;
}
removeSesion()async{
  final shared = await _prefs;
  var eli = shared.remove(session);
  return eli;
}
guardarEmail(String email) async{
  final shared = await _prefs;
  var guardar = shared.setString(emailValidar, email);
  return guardar;
}
obtenerEmail() async {
  final shared = await _prefs;
  return shared.getString(emailValidar);
}
guardarDatosCambiarContrasenia(cambiar) async{
  final shared = await _prefs;
  var guardar = shared.setString(cambiarClave, json.encode(cambiar));
  return guardar;
}
obtenerDatosCambiarContrasenia() async {
  final shared = await _prefs;
  return shared.getString(cambiarClave);
}

guardarCodigo(codigoOtp) async {
  final shared = await _prefs;
  shared.setString(codigo, codigoOtp);
}
obtenerCodigo() async{
  final shared = await _prefs;
  return shared.getString(codigo);
}
guardarPerfilChat(Map data) async {
  final shared = await _prefs;
  shared.setString(perfilChat, json.encode(data));
}
obtenerPerfilChat() async {
  final shared = await _prefs;
  return shared.getString(perfilChat);
}