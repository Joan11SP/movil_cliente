import 'package:flutter/widgets.dart';

const String ip = '192.168.1.16' ;
const int port = 4000;
const int portApi = 4500;
//const String urlSocket = 'https://chat-nanny.herokuapp.com/';
const String urlSocket = 'http://fintech.kradac.com:4501/';
const String urlApi = 'http://fintech.kradac.com:4500/api';
//const String urlApi = 'https://ninieras.herokuapp.com/api';
const String secretKeySocket = '@ llave para encritar datos por sockets @';
const String secretKeyApi = "@ key para encriptar datos via api rest @";
const int peticionCorrecta= 1;
const int sinCuestionario= 2;
const int pendiente = 3;
const int sinPermisosApp = 7;
const int peticionErronea = 0;
const bool respuestaEncriptada = false;
const int tiempoToast = 2;
const int servicioconcluyo = 4;
const String mensajeError='Ocurrio un error, intenta más tarde';
const String registroPersona= '/completar-registro';
const String recuperarContrasenia = '/recuperar-contrasenia';
const String camposVacios = 'Los campos no pueden quedar vacios';
const String dominio = 'google.com';
const String sinInternet = 'Sin conexión a internet, intente más tarde';
const String imagenPrueba = 'assets/images/Profile Image.png';
const int archivosPermitidos = 3;
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
const String imageMary = 'assets/images/mary.png';
const int otpLoginCode = 9;
const int otpCrearCUentaCode = 8;
Map cabecera = {
  'app_nombre':'app_family',
  
};