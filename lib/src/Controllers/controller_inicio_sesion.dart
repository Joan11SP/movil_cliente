import 'package:flutter/cupertino.dart';
import 'package:movil_nanny/src/Services/shared_preferences.dart';
import 'package:movil_nanny/src/screens/home/home_screen.dart';
 
login(Map facebookprofile,context) async {
    try {
      await guardarPerfil(facebookprofile);
      Navigator.of(context).popAndPushNamed(HomeScreen.routeName);
    } on Exception catch (e) {
      print('ERROR => ${e.toString()}');
    }
    
}
