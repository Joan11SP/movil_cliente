import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_nanny/src/Api/apiPersona.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configEncrypt.dart';
import 'package:movil_nanny/src/Config/configSharedPreferences.dart';
import 'package:movil_nanny/src/Models/model_persona.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/screens/cuestionario/cuest_screen.dart';
import 'package:movil_nanny/src/screens/home/home_screen.dart';
import 'package:movil_nanny/src/screens/sign_in/controller/controller_sesion.dart';

registrarPersona(Persona persona,context) async {
  try {
    http.Response response = await nuevaPersonaApi(persona);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    Navigator.pop(context);
    if (response.statusCode == 200) {
       
      if (body['ok'] == otpCrearCUentaCode){
        var codigo = '';
        validarOtpInicio(codigo,context,body);
      }
      else{
        await guardarLogin(body);
        if(body['ok'] == peticionCorrecta){         
          Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (Route<dynamic> route) => false);
        }
        if(body['ok'] == sinCuestionario){
          Navigator.of(context).pushNamedAndRemoveUntil(CuestionarioScreen.routeName, (Route<dynamic> route) => false);
        }
      }
       
    }
    mostrarMensaje(body['mensaje'], context, tiempoToast);
  } on Exception catch (e) {
    print(e);
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
