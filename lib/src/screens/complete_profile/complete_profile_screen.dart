import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';

import 'components/body.dart';

class CompleteProfileScreen extends StatelessWidget {
  static String routeName = "/complete_profile";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Registrarse',style: stiloText(),),
      ),
      body: Body(),
    );
  }
}
