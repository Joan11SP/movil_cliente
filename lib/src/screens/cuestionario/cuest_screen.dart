import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/cuestionario/cuest_body.dart';
import 'package:movil_nanny/src/size_config.dart';

class CuestionarioScreen extends StatelessWidget {
  static String routeName= '/cuestionario-nanny';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Center(child: Text('Información',style: stiloText(),)),
      ),
      body: CuestionarioBody(),
    );
  }
}