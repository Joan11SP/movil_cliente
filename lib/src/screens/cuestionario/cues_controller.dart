import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_nanny/src/Api/apiPersona.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configEncrypt.dart';
import 'package:movil_nanny/src/Config/configSharedPreferences.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/screens/home/home_screen.dart';
import 'package:movil_nanny/src/screens/home/pendiente.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http_parser/http_parser.dart';

controllerGuetCuestionario(context) async {
  var preguntas = [];
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await obtenerCuestionario(datos['persona']['identificador']);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
      preguntas = body['respuesta'];
    }else{
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
    return preguntas;
  } on Exception catch (e) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return preguntas;
  }
}

controllerGuardarCuestionario(List cuestionario,imagenes,context) async {
  try {

    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    var req = http.MultipartRequest('POST', Uri.parse('$urlApi/crear-cuestionario-niniera'));
    
    req.fields.addAll({
      'respuestas':json.encode(cuestionario),
      'id_niniera':datos['persona']['identificador']
    });
    for (var i = 0; i < imagenes.length; i++) {
      req.files.add(await http.MultipartFile.fromPath(
        'add_archivos',
        imagenes[i]['imagen'],
        contentType: MediaType('image', imagenes[i]['extension']),
      ));
    }
    //http.Response response = await crearCuestionarioApi(test);

    http.StreamedResponse res = await req.send();
    esconderLoading(context);
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    var respuesta,body;
    if (respuestaEncriptada) {
        respuesta = decryptAESCryptoJS(responseString, secretKeyApi);
        body = json.decode(respuesta);
    } else {
        body = json.decode(responseString);
    }
    if(body['ok'] == pendiente){
      datos['ok'] = pendiente;
      await guardarLogin(datos);
      Navigator.of(context).pushNamedAndRemoveUntil(PendienteScreen.routeName, (Route<dynamic> route) => false);
    }
    else if(body['ok'] == peticionCorrecta){
      datos['ok'] = peticionCorrecta;
      await guardarLogin(datos);
      Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (Route<dynamic> route) => false);
    }
    else{
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
    
  } on Exception catch (e) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
controllerPendiente(context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);

    http.Response response = await validarPendinte(datos['persona']['identificador']);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if(body['ok'] == peticionCorrecta){
      datos['ok'] = peticionCorrecta;
      await guardarLogin(datos);
      Navigator.popAndPushNamed(context,HomeScreen.routeName);
    }
    else{
      mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
  } on Exception catch (e) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
pedirPersmisos() async {
  Map<Permission, PermissionStatus> statuses = await [
    Permission.storage,
  ].request();
  return statuses;
}
/*

if(cuestionario[i]['justificar']) { 
          respuestas.add({
            'info': cuestionario[i]['opciones']['info'],
            'respuesta': cuestionario[i]['opciones']['opcion'],
            'justificacion': cuestionario[i]['opciones']['justificacion'],
            'tipo': cuestionario[i]['tipo']
          });
        }
        else if (cuestionario[i]['tipo'] == 'abierta'){
          respuestas.add({
            'info': cuestionario[i]['opciones']['info'],
            'respuesta': cuestionario[i]['opciones'],
            'tipo': cuestionario[i]['tipo']
          });
        }
        else{
          respuestas.add({
            'info': cuestionario[i]['opciones']['info'],
            'respuesta': cuestionario[i]['opciones']['opcion'],
            'tipo': cuestionario[i]['tipo']
          });
        }
*/