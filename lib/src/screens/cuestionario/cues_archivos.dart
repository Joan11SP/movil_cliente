import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/helper/keyboard.dart';
import 'package:movil_nanny/src/screens/cuestionario/cues_controller.dart';
import 'package:movil_nanny/src/size_config.dart';
import 'dart:io';
import 'package:path/path.dart' as path;

import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class AddArchivos extends StatefulWidget {
  static String routeName = '/add_archivos';
  @override
  _AddArchivosState createState() => _AddArchivosState();
}

class _AddArchivosState extends State<AddArchivos> {
  var imagenes = [];
  var enviarimagenes = [];
  var preguntas;
  File imagen;
  final picker = ImagePicker();
  String info =
      'Necesitamos que adjuntes tu identificaciòn por ambos lados y de ser posible el Titulo de tu profesiòn';
  bool addArchivos = true;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    preguntas = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text(
          'Subir archivos',
          style: stiloText(),
        ),
        centerTitle: true,
        backgroundColor: kPrimaryColor,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: 10, left: 15),
                  child: Text(
                    info,
                    style: estiloText(),
                  ),
                ),
                mostrarImagenes()
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    color: kPrimaryColor,
                    onPressed: () => validar(),
                    child: Text('Agregar archivo',style: stiloText(),),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  RaisedButton(
                    color: kPrimaryColor,
                    onPressed: () async {
                      mostrarLoading(context,'Cargando');
                      await controllerGuardarCuestionario(preguntas,enviarimagenes,context);
                      // if(imagenes.length>=archivosPermitidos-1){
                      //   mostrarLoading(context,'Cargando');
                      //   controllerGuardarCuestionario(preguntas,imagenes,context);
                      // }
                      // else
                      //   mostrarMensaje('Añade por lo menos ${archivosPermitidos-1} archivos', context, 2);
                    },
                    child: Text('Continuar',style: stiloText()),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     addArchivos ? validar() : print('hola');
      //   },
      //   child: Icon(addArchivos ? Icons.add_a_photo : Icons.send),
      //   backgroundColor: kPrimaryColor,
      // ),
    );
  }

  validar() async {
    if (imagenes.length < archivosPermitidos) {
      mostrarLoading(context, 'Cargando...');
      var status = await Permission.storage.status;
      if (status.isGranted) {
        selectImageGallery();
      } else {
        mostrarMensaje('Habilita los permisos para continuar', context, 3);
        pedirPersmisos();
        Navigator.pop(context);
      }
    } else {
      mostrarMensaje(
          'Número de imagenes permitidas $archivosPermitidos \nPresiona sobre la imagen para eliminar',
          context,
          3);
    }
  }

  mostrarImagenes() {
    return ListView.builder(
      itemCount: imagenes.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      reverse: true,
      itemBuilder: (BuildContext context, int i) {
        return GestureDetector(
          onTap: () {
            preguntarEliminarImagen(i);
          },
          child: Image.file(imagenes[i],
              height: MediaQuery.of(context).size.height * 0.5,
              width: MediaQuery.of(context).size.width * 0.5),
        );
      },
    );
  }

  preguntarEliminarImagen(int i) async {
    try {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('¿Esta seguro de eliminar la imagen?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  onPressed: () {
                    Navigator.pop(context);
                    KeyboardUtil.hideKeyboard(context);
                  },
                ),
                FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    setState(() {
                      imagenes.removeAt(i);
                      enviarimagenes.removeAt(i);
                      Navigator.pop(context);
                      KeyboardUtil.hideKeyboard(context);
                      if (imagenes.length <= 0) {
                        setState(() {
                          addArchivos = true;
                        });
                      }
                    });
                  },
                )
              ],
            );
          });
    } on AssertionError catch (e) {
      print(e);
    }
  }

  selectImageGallery() async {
    try {
      final img = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        imagen = File(img.path);
        imagenes.add(imagen);
        var extension = path.extension(imagen.path);
        print('EXTENSION $extension');
        addArchivos = false;
        enviarimagenes.insert(0, {'imagen': imagen.path, 'extension': extension});
      });
      Navigator.pop(context);
    } on NoSuchMethodError catch (e) {
      Navigator.pop(context);
    } on PlatformException catch (e) {
      Navigator.pop(context);
    }
  }
}
