import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/components/default_button.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/cuestionario/cues_archivos.dart';
import 'package:movil_nanny/src/screens/cuestionario/cues_controller.dart';
import 'package:movil_nanny/src/size_config.dart';

class CuestionarioBody extends StatefulWidget {
  @override
  _CuestionarioBodyState createState() => _CuestionarioBodyState();
}

class _CuestionarioBodyState extends State<CuestionarioBody> {
  int value; bool multiple = false;int count = 0;
  bool abierta= false;
  var cuestionario = [];
  TextEditingController descripcion = TextEditingController();
  TextEditingController justificacion = TextEditingController();
  int posicion;
  List preguntas = [];
  var coloropciones = 4278232733;
  getPreguntas() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context, 'Cargando');
    preguntas = await controllerGuetCuestionario(context);
    setState(() {});
  }

  @override
  void initState() {
    getPreguntas();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: preguntas.length > 0
            ? Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(height: getProportionateScreenHeight(20)),
                  Text('Pregunta ${count + 1} de ${preguntas.length}',style: estiloText(),),
                  SizedBox(height: getProportionateScreenHeight(40)),
                  Container(
                    width: getProportionateScreenWidth(350),
                    child: Text('${preguntas[count]['pregunta']}',style: estiloText(),),
                  ),
                  SizedBox(height: getProportionateScreenHeight(40)),
                  Container(
                      child: preguntas[count]['tipo'] == 'cerrada'
                              ? opcionCerrada()
                              : preguntas[count]['tipo'] == 'multiple' ? opcionMultiple() : opcionEscribir()
                          ),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: count != preguntas.length - 1
                          ? Padding(
                              padding:
                                  EdgeInsets.only(left: 50, right: 50, top: 30),
                              child: DefaultButton(
                                  text: 'Siguiente', press: () => siguiente()),
                            )
                          : Padding(
                              padding:
                                  EdgeInsets.only(left: 50, right: 50, top: 30),
                              child: DefaultButton(
                                text: 'Terminar Cuestionario',
                                press: () {
                                  bool validar = validarUltimaPreguna();
                                  if (validar) {
                                    Navigator.pushNamed(context, AddArchivos.routeName,arguments: preguntas);
                                  } else {
                                    mostrarMensaje('Responde la pregunta', context, 2);
                                  }
                                },
                              ),
                            ))
                ],
              )
            : Container(),
      ),
    );
  }

  alertPreguntar() {
    return showDialog(
        context: context,
        child: AlertDialog(
          content: Text('Enviar información'),
          title: Text('Terminar cuestionario'),
          actions: [
            FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Cancelar')),
            FlatButton(
                onPressed: () {
                  /*if (descripcion.text.toString().trim().length > 10 &&
                      descripcion.text.toString().trim().length < 256) {
                    cuestionario.add({
                      'respuesta': descripcion.text,
                      'pregunta': preguntas[count]['pregunta'],
                      'tipo': 'descripcion'
                    });

                    mostrarLoading(context, 'Enviando información');
                    controllerGuardarCuestionario(cuestionario, context);
                  }*/
                  print(preguntas);
                },
                child: Text('Aceptar')),
          ],
        ));
  }

  opcionMultiple() {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: preguntas[count]['opciones'].length,
        itemBuilder: (context, i) {
          return CheckboxListTile(
              key: Key(i.toString()),
              value: preguntas[count]['opciones'][i]['select'],
              activeColor: Color(coloropciones),
              checkColor: Colors.white,
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (bool value) {
                setState(() {
                  multiple = false;                  
                  preguntas[count]['opciones'][i]['select'] = value;
                  for (var j = 0; j < preguntas[count]['opciones'].length; j++) 
                    if(preguntas[count]['opciones'][j]['select']==true) {multiple = true;}
                  
                });
              },
              title: Text(preguntas[count]['opciones'][i]['opcion']));
        });
  }

  opcionCerrada() {
    return Column(
      children: [
        ListView.builder(
            shrinkWrap: true,
            itemCount: preguntas[count]['opciones'].length,
            itemBuilder: (context, i) {
              return RadioListTile(
                  key: Key(i.toString()),
                  value: i,
                  title: Text(preguntas[count]['opciones'][i]['opcion']),
                  groupValue: value,
                  activeColor: Color(coloropciones),
                  onChanged: (v) {
                    setState(() {
                      if (preguntas[count]['justificar']) posicion = i;
                      value = i;
                      for (var i = 0;
                          i < preguntas[count]['opciones'].length;
                          i++) {
                        preguntas[count]['opciones'][i]['select'] = false;
                      }
                      preguntas[count]['opciones'][i]['select'] = true;
                    });
                  });
            }),
        if (preguntas[count]['justificar'])
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20, top: 20),
            child: TextFormField(
              controller: justificacion,
              decoration: InputDecoration(
                  labelText: "Justifica tu respuesta",
                  hintText:
                      'Puedes escribir que titulo tienes, tu experiencia, donde has trabajado,etc'),
              maxLength: 250,
              maxLines: 3,
            ),
          )
      ],
    );
  }

  opcionEscribir(){
    return Padding(
      padding:
          EdgeInsets.only(left: 20, right: 20, top: 20),
      child: TextFormField(
        controller: descripcion,
        onChanged: (value){
          setState(() {
            preguntas[count]['opciones'] = descripcion.text;
            abierta = true;
          });
        },
        maxLines: 4,
        maxLength: 200,
      ),
    );

  }
  
  siguiente() {
    if (preguntas[count]['tipo'] == 'cerrada') {
      if (value != null) {
        justificaciones();
      } else {
        mostrarMensaje('Responde la pregunta', context, 2);
      }
    }
    if (preguntas[count]['tipo'] == 'multiple') {
      if (multiple) {
        multiple = false;
        count++;
      } else {
        mostrarMensaje('Responde la pregunta', context, 2);
      }
    }
    if (preguntas[count]['tipo'] == 'abierta') {
      if (abierta) {
        abierta = false;
        descripcion.clear();
        count++;
      } else {
        mostrarMensaje('Responde la pregunta', context, 2);
      }
    }
    setState(() {});
  }

  justificaciones() {
    if (preguntas[count]['justificar']) {
      if (justificacion.text.toString().trim().length <= 0) {
        mostrarMensaje('Escribe una justificaciòn', context, 3);
      } else {
        preguntas[count]['opciones'][posicion]['justificacion'] = justificacion.text.toString();
        count++;
        value = null;
      }
    } else {
      value = null;
      count++;
    }
  }
  
  bool validarUltimaPreguna(){
    bool validar = false;
    if (preguntas[count]['tipo'] == 'cerrada') {
      if (value != null) {
        validar = true;
      }
    }
    if (preguntas[count]['tipo'] == 'multiple') {
      if (multiple) {
        validar = true;
      }
    }
    if (preguntas[count]['tipo'] == 'abierta') {
      if (abierta) {
        validar = true;
        descripcion.clear();
      }
    }
    return validar;
  }
}
