import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_nanny/src/Api/apiPersona.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configEncrypt.dart';
import 'package:movil_nanny/src/Config/configSharedPreferences.dart';
import 'package:movil_nanny/src/Models/model_persona.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/screens/cuestionario/cuest_screen.dart';
import 'package:movil_nanny/src/screens/home/home_screen.dart';
import 'package:movil_nanny/src/screens/home/pendiente.dart';

controllerLoginPersona(Login login, context) async {
  try {
    http.Response response = await loginPersonaApi(login);
    var respuesta;

    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    respuesta = json.decode(response.body);

    Navigator.pop(context);
    print(respuesta);
    if (response.statusCode == 200) {
      if (respuesta['ok'] == peticionCorrecta) {
        await guardarLogin(respuesta);
        Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      }
      if (respuesta['ok'] == sinCuestionario) {
        await guardarLogin(respuesta);
        Navigator.of(context)
            .pushReplacementNamed(CuestionarioScreen.routeName);
      }
      if (respuesta['ok'] == pendiente) {
        await guardarLogin(respuesta);
        Navigator.of(context).pushReplacementNamed(PendienteScreen.routeName);
      }
      if (respuesta['ok'] == otpLoginCode) {
        var codigo = '';
        validarOtpInicio(codigo, context, respuesta);
      }
    }
    mostrarMensaje(respuesta['mensaje'], context, tiempoToast);
  } on Exception catch (e) {
    print('ERROR => $e');
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}

validarOtpInicio(codigo, context, respuesta) {
  ingresarCodigoOtp(context, codigo, (res) async {
    codigo = res;
    mostrarLoading(context, 'Enviando...');
    if (res.length == 6) {
      var body = {
        "motivo": respuesta['motivo'],
        "id_motivo": respuesta['id_motivo'],
        "id": respuesta['id'],
        "codigo": codigo.toString()
      };
      var res = await obtenerRecursoApi('validar-otp', body, headerNoJosn);
      esconderLoading(context);
      if(res != 0){
        await guardarLogin(res);
        if(res['ok'] == peticionCorrecta){         
            Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (Route<dynamic> route) => false);
        }
        if(res['ok'] == sinCuestionario){
          Navigator.of(context).pushNamedAndRemoveUntil(CuestionarioScreen.routeName, (Route<dynamic> route) => false);
        }
        if (res['ok'] == pendiente) {
          Navigator.of(context).pushNamedAndRemoveUntil(PendienteScreen.routeName, (Route<dynamic> route) => false);
        }
        else {
          mostrarMensaje(res['mensaje'], context, tiempoToast);
        }
      }
      else
        mostrarMensaje(mensajeError, context, tiempoToast);
    }
  });
}
