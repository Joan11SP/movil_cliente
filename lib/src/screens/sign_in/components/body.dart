import 'package:flutter/material.dart';
import 'package:movil_nanny/src/Api/apiPersona.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/components/no_account_text.dart';
import 'package:movil_nanny/src/components/socal_card.dart';
import 'package:movil_nanny/src/constants.dart';
import '../../../size_config.dart';
import 'sign_form.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  ///Persona persona= new Persona();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(30)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.10),
                Row(
                  children: [
                    SizedBox(width: getProportionateScreenHeight(20)),
                    Text(
                      "Servicio de niñeras",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getProportionateScreenWidth(28),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    Image.asset(
                      imageMary,
                      color: kSecondaryColor,
                      height: 65,
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                SignForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // SocalCard(
                    //   icon: "assets/icons/facebook-2.svg",
                    //   press: () async {
                    //     /*mostrarLoading(context);
                    //     Map facebookprofile = await loginFacebook(context);

                    //     if(facebookprofile != null){
                    //       await login(facebookprofile,context);

                    //     }*/
                    //   },
                    // ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                NoAccountText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
