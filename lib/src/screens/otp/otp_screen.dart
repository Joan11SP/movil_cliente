import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/size_config.dart';
import 'components/body.dart';

class OtpScreen extends StatelessWidget {
  static String routeName = "/otp";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
       backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text("OTP Verificación",style: stiloText(),),
      ),
      body: Body(),
    );
  }
}
