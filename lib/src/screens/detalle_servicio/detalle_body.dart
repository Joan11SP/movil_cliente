import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/size_config.dart';

class DetalleBody extends StatelessWidget {

  String iconUser = 'assets/icons/User.svg';
  String iconTime = 'assets/icons/cronometro.svg';
  String iconComent = 'assets/icons/comentario.svg';
  String iconStar = 'assets/icons/Star Icon.svg';
  String iconCalendar = 'assets/icons/calendario.svg';
  var detalles = [];

  @override
  Widget build(BuildContext context) {
    detalles = ModalRoute.of(context).settings.arguments;
    return SingleChildScrollView(
      physics: ClampingScrollPhysics(),
      child: Column(
        children: [
          SizedBox(height: getProportionateScreenHeight(20),),
          Text('Total de Servicios realizados:'+ " " + detalles.length.toString(),style: estiloText(),),
          SizedBox(height: getProportionateScreenHeight(20),),
          Container(
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: detalles.length,
              itemBuilder: (_,i){
                return Card(
                  clipBehavior: Clip.antiAlias,
                  elevation:5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Column(
                    children: [
                      ListTile(
                        leading: SvgPicture.asset(iconUser,
                          color: kPrimaryColor,
                          width: 22,),
                        title: Text('Nombres'),
                        subtitle: Text(detalles[i]['nombres']),
                      ),
                      ListTile(
                        leading: SvgPicture.asset(iconTime,
                          color: kPrimaryColor,
                          width: 22,),
                        title: Text('Horas realizadas'),
                        subtitle: Text(detalles[i]['horas'].toString()),
                      ),
                      ListTile(
                        leading: SvgPicture.asset(iconCalendar,
                          color: kPrimaryColor,
                          width: 22,),
                        title: Text('Fechas'),
                        subtitle: Text(detalles[i]['fechas']),
                      ),
                      detalles[i].containsKey('comentario') ?
                      ListTile(
                        leading: SvgPicture.asset(iconComent,
                          color: kPrimaryColor,
                          width: 22,),
                        title: Text('Comentario recibido'),
                        subtitle: Text(detalles[i]['comentario']),
                      ):Text(''),
                      detalles[i].containsKey('puntuacion') ?
                      ListTile(
                        leading: Icon(Icons.star_border,color: kPrimaryColor,size: 25,),
                        title: Text('Puntuacion recibida'),
                        subtitle: Text(detalles[i]['puntuacion'].toString()) ,
                      ):Text(''),
                    ],
                  ),
                );
              },
            ),
          ),

          SizedBox(height: getProportionateScreenHeight(20),),
        ],
      ),
    );
  }
}
