import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/detalle_servicio/detalle_body.dart';
import 'package:movil_nanny/src/size_config.dart';

class DetalleServicio extends StatelessWidget {
  static String routeName = "/detalle_servicio";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
      elevation: 15.0,
      centerTitle: true,
      backgroundColor: kPrimaryColor,
      title: Text(
        'Servicios Realizados',
        style: stiloText(),
      ),
    ),
      body: SafeArea(child: DetalleBody()),
    );
  }
}
