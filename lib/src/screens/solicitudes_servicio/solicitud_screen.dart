import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/solicitudes_servicio/solicitud_body.dart';
import 'package:movil_nanny/src/size_config.dart';

class SolicitudesScreen extends StatelessWidget {
  static String routeName = '/solicitudes-servicio';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Solicitudes de Servicio',style: stiloText(),),
      ),
      body:SolcitudesBody(),
    );
  }
}