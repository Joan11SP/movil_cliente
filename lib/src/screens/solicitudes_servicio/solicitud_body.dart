import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/solicitudes_servicio/solicitud_controller.dart';
import 'package:movil_nanny/src/size_config.dart';
import '../../Config/configSharedPreferences.dart';
class SolcitudesBody extends StatefulWidget {
  @override
  _SolcitudesBodyState createState() => _SolcitudesBodyState();
}

class _SolcitudesBodyState extends State<SolcitudesBody> {
  var propuestas;
  var datosId;
  getPropuesta() async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context, 'Cargando...');
    var data = await controllerGetSolicitudes(context);
    print(data);
    if (data.length > 0) {
      propuestas = data;
      datosId = data[0];
    }
    print('DATOSID $datosId');
    setState(() {});
  }

  @override
  void initState() {
    getPropuesta();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: propuestas != null
            ? Padding(
              padding: EdgeInsets.only(top: 20),
              child: Container(
                  child: ListView.builder(
                    itemCount: propuestas.length,
                    itemBuilder: (_, i) {
                      return Card(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ListTile(
                              leading: propuestas[i]['persona'][0]['photo'] !=
                                      null
                                  ? CircleAvatar(
                                      radius: 25,
                                      child: ClipOval(
                                        child: Image.network(
                                            propuestas[i]['persona'][0]['photo'],
                                            errorBuilder: (context,
                                                Object exception,
                                                StackTrace stackTrace) {
                                          return Image.asset(imagenPrueba);
                                        }),
                                      ),
                                    )
                                  : CircleAvatar(
                                      radius: 25,
                                      foregroundColor:
                                          Theme.of(context).primaryColor,
                                      backgroundColor: Colors.grey,
                                      backgroundImage: AssetImage(imagenPrueba)),
                              title: Text(propuestas[i]['persona'][0]['first_name'].toString() +
                                  " " + propuestas[i]['persona'][0]['last_name'].toString()),
                              subtitle: Column(
                                children: [
                                  Text('Descripcón: ',
                                      style:
                                          TextStyle(fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: getProportionateScreenHeight(10),
                                  ),
                                  Container(
                                    child: Text(
                                      propuestas[i]['descripcion'].toString(),
                                    ),
                                  ),
                                  SizedBox(
                                    height: getProportionateScreenHeight(10),
                                  ),
                                  Text('Fechas: ',
                                      style:
                                          TextStyle(fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: getProportionateScreenHeight(10),
                                  ),
                                  Container(
                                      child: Text('desde ' +
                                          propuestas[i]['fecha_inicio']
                                              .toString() +
                                          " hasta " +
                                          propuestas[i]['fecha_fin'].toString()))
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                SizedBox(
                                  height: getProportionateScreenHeight(150),
                                ),
                                FlatButton(
                                    color: kPrimaryColor,
                                    onPressed: () async  {
                                      mostrarLoading(context,'Enviando...');
                                      Map crear = {
                                        'id_familia':propuestas[i]['id_familia'],
                                        'id_solicitud':propuestas[i]['_id']
                                      };
                                      controllerCrearPropuesta(crear,context);
                                    },
                                    child: Text(
                                      'Proponer Servicio',
                                      style: TextStyle(color: Colors.white),
                                    )),
                                SizedBox(
                                  width: 10,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 8.0),
                                  child: FlatButton(
                                      color: kPrimaryColor,
                                      onPressed: () {
                                        mostrarLoading(context,'Cargando...');
                                        buscarMasInfoSolicitud(propuestas[i]['id_familia'], context);
                                      },
                                      child: Text('Ver más',
                                          style: TextStyle(color: Colors.white))),
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
            )
            : Container(
                alignment: Alignment.center,
                child: Center(
                    child: Text('Aun no han solicitado el servicio',
                        textAlign: TextAlign.center))));
  }
}
