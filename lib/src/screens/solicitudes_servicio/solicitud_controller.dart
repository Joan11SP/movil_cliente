import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_nanny/src/Api/apiPersona.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configEncrypt.dart';
import 'package:movil_nanny/src/Config/configSharedPreferences.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/screens/info_solicitud/info_solicitud_screen.dart';
import '../home/home_screen.dart';

controllerGetSolicitudes(context) async {
  var solicitudes=[];
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response =  await buscarSolicitud(datos['persona']['identificador']);
    var respuesta;

    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(response.body);
    solicitudes = respuesta['respuesta'];
    return solicitudes;
  } on Exception catch (e) {    
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return solicitudes=[];
  }
}

buscarMasInfoSolicitud(String id, context) async {
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await infoSolicitudApi(datos['persona']['identificador'],id);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(body);
    var masinfo = respuesta['respuesta'];
    if(masinfo != null){
      Navigator.of(context).pushNamed(InfoSolicitudScreen.routeName,arguments: masinfo);
    }
  } on SocketException catch (_) {
    esconderLoading(context);
    mostrarMensaje(sinInternet, context, tiempoToast);
  } on Exception catch (_) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}


controllerCrearPropuesta(Map crear,context) async {
try {
    var datos = await obtenerSesion();
    var per = json.decode(datos);
    crear['id_niniera'] = per['persona']['identificador'];
    http.Response response = await crearPropuestaApi(crear);
    var respuesta, body;
    body = response.body;
    if (respuestaEncriptada) {
      respuesta = decryptAESCryptoJS(response.body, secretKeyApi);
      body = json.decode(respuesta);
    } else {
      body = json.decode(body);
    }
    esconderLoading(context);
    if (response.statusCode == 200 && body['ok'] == peticionCorrecta) {
      Navigator.popAndPushNamed(context, HomeScreen.routeName);
      mostrarMensaje(body['mensaje'],context, tiempoToast);
    }else{ 
    mostrarMensaje(body['mensaje'], context, tiempoToast);
    }
 } on Exception catch (e) {
    Navigator.pop(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
  }
}
