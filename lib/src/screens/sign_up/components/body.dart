import 'package:flutter/material.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/components/socal_card.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/size_config.dart';

import 'sign_up_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
                Text("Registrar Cuenta", style: headingStyle),
                Text(
                  "Completa la información",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                SignUpForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // SocalCard(
                    //   icon: "assets/icons/google-icon.svg",
                    //   press: () {},
                    // ),
                    // SocalCard(
                    //   icon: "assets/icons/facebook-2.svg",
                    //   press: () {},
                    // ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                Text(
                  'Al continuar aceptas los terminos y condiciones',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
                SizedBox(height: getProportionateScreenHeight(40)),
                    Image.asset(
                      imageMary,
                      color: kSecondaryColor,
                      height: 80,
                    ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
