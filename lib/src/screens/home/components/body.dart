import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_nanny/src/Services/firebase_notificacion.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/home/home_controller.dart';
import 'package:movil_nanny/src/screens/solicitudes_servicio/solicitud_screen.dart';
import 'package:movil_nanny/src/size_config.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var detalle = [];
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
  String iconStar = 'assets/icons/Star Icon.svg';
  String iconComentario = 'assets/icons/comentario.svg';
  String iconServicio = 'assets/icons/solicitar.svg';
  final notificacion = new Notificacion();
  getDetalle() async {
    await Future.delayed(Duration(milliseconds: 1));
    mostrarLoading(context, 'Cargando...');
    detalle = await controllerGetDetalle(context);
    setState(() {});
  }

  @override
  void initState() {
    getDetalle();
    notificacion.initNotifications(context);
    notificacion.mensaje.listen((data) {
      print(data);
      if (data['pantalla'] == 'solicitud') {
        navigatorKey.currentState.pushNamed(SolicitudesScreen.routeName);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: getProportionateScreenHeight(20),
            ),
            Container(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: detalle.length,
                    itemBuilder: (_, i) {
                      return Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: FlatButton(
                          padding: EdgeInsets.all(20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          color: Color(0xFFF5F6F9),
                          onPressed: () async {
                            if (detalle[i]['ver'] == 'serv') {
                              mostrarLoading(context, 'Cargando');
                              getDatosDetalles(context);
                            }
                            if (detalle[i]['ver'] == 'en_proceso') {
                              controllerGetServicioProceso(
                                  context, detalle[i]['data']);
                            }
                          },
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                detalle[i]['ver'] == 'star'
                                    ? iconStar
                                    : detalle[i]['ver'] == 'comen'
                                        ? iconComentario
                                        : iconServicio,
                                color: kSecondaryColor,
                                width: 22,
                              ),
                              SizedBox(width: 20),
                              Expanded(
                                  child: Text(detalle[i]['name'].toString())),
                              Text(detalle[i]['total'].toString()),
                              detalle[i]['ver'] == 'serv'
                                  ? Icon(Icons.arrow_forward_ios_outlined,
                                      color: kSecondaryColor)
                                  : Text('')
                            ],
                          ),
                        ),
                      );
                    })),
          ],
        ),
      ),
    );
  }

  refrescarDatos() {
    return RefreshIndicator(child: Container(), onRefresh: getDetalle());
  }
}
