import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/components/default_button.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/cuestionario/cues_controller.dart';
import 'package:movil_nanny/src/size_config.dart';

class PendienteScreen extends StatelessWidget {

  static String routeName = '/en-espera';
  String informacion = 'Por el momento te encuentras a la espera de que habiliten tu cuenta, pronto te informaremos';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('En espera' ,style: stiloText(),),
      ),
      body: Padding(
        padding: EdgeInsets.only(
            left: getProportionateScreenHeight(20),top: getProportionateScreenWidth(150),
            right: getProportionateScreenHeight(20),bottom: getProportionateScreenWidth(150)
        ),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(informacion,style: estiloText(),),
                ),
              ),
              SizedBox(height: getProportionateScreenHeight(20),),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: DefaultButton(
                  text: 'Actualizar informaciòn',
                  press: (){
                    mostrarLoading(context,'Cargando...');
                    controllerPendiente(context);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
