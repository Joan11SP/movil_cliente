import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:movil_nanny/src/Api/apiPersona.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configEncrypt.dart';
import 'package:movil_nanny/src/Config/configSharedPreferences.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/screens/detalle_servicio/detalle_screen.dart';
import 'package:movil_nanny/src/screens/solicitudes_servicio/solicitud_controller.dart';

controllerGetDetalle(context) async {
  var detalles=[];
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await verDetalleServicios(datos['persona']['identificador']);    
    var respuesta,body;
    body = response.body;
    if(respuestaEncriptada){
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(body);
    detalles = respuesta['respuesta'];
    return detalles;
  } on Exception catch (e) {
    print('ERROR ===> $e');
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return detalles=[];
  }

}
controllerGetServicios(context) async {
  var detalles=[];
  try {
    var persona = await obtenerSesion();
    var datos = json.decode(persona);
    http.Response response = await detalleServicio(datos['persona']['identificador']);    
    var respuesta,body;
    body = response.body;
    if(respuestaEncriptada){
      body = decryptAESCryptoJS(body, secretKeyApi);
    }
    esconderLoading(context);
    respuesta = json.decode(body);
    detalles = respuesta['respuesta'];
    return detalles;
  } on Exception catch (e) {
    esconderLoading(context);
    mostrarMensaje(mensajeError, context, tiempoToast);
    return detalles=[];
  }

}

getDatosDetalles(context) async {
  var datos = await controllerGetServicios(context);

  if(datos.length > 0){
    Navigator.of(context).pushNamed(DetalleServicio.routeName,arguments:datos);
  }else{
    mostrarMensaje('No tiene servicios realizados', context, 3);
  }
}

controllerGetServicioProceso(context,proceso){
  mostrarLoading(context,'Enviando...');
  
  buscarMasInfoSolicitud(proceso['familia'], context);
}