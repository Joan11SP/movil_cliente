import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/components/coustom_bottom_nav_bar.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/enums.dart';
import 'package:movil_nanny/src/size_config.dart';

import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(      
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Nanny' ,style: stiloText(),),
      ),
      body: SafeArea(child: Body()),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}
