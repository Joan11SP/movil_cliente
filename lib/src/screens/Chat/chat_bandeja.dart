import 'dart:convert';
import 'package:movil_nanny/src/Services/chat_socket.dart';
import 'package:movil_nanny/src/Services/provider.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/Chat/chat_screen.dart';
import 'package:provider/provider.dart';

import '../../Config/configSharedPreferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_nanny/src/Models/modelChat.dart';

class BandejaChat extends StatefulWidget {
  static String routeName = 'bandeja-chat';
  @override
  _BandejaChatState createState() => _BandejaChatState();
}

class _BandejaChatState extends State<BandejaChat> {
  String nuevousuario = 'nuevo-usuario-conectado';
  String chatActuales = 'get-chat-service';
  String servicioChat = 'find-chat-service';
  String idUser;
  var chat = new Chat();
  int totalChat = 0;
  List _servicioChat;

  getId(socket) async {
    await Future.delayed(Duration(milliseconds: 2));
    mostrarLoading(context, 'Cargando...');
    var datos = await obtenerSesion();
    var per = json.decode(datos);
    idUser = per['persona']['identificador'];

    if (idUser != null) {
      pedirChat(socket);
    }
    setState(() {});
  }

  @override
  void initState() {
    final socketService = Provider.of<ChatService>(context, listen: false);
    final provider = Provider.of<ProviderInfor>(context, listen: false);
    getId(socketService);

    socketService.scoket
        .on(servicioChat, (data) => {obtenerChat(data, provider)});
    super.initState();
  }

  obtenerChat(dynamic data, provider) {
    if (mounted) {
      _servicioChat = data;
      totalChat = data.length;
      print(_servicioChat);
      if (_servicioChat.length > 0) {
        if (_servicioChat[0]['chat'].length > 0) if (_servicioChat[0]['chat'][0]
                    ['message']
                .indexOf('https:') !=
            -1) {
          provider.mostrarUltimoMensaje(
              'Foto', _servicioChat[0]['chat'][0]['emisor']);
        } else {
          provider.mostrarUltimoMensaje(_servicioChat[0]['chat'][0]['message'],
              _servicioChat[0]['chat'][0]['emisor']);
        }
      }
      setState(() {});
      esconderLoading(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final socketService = Provider.of<ChatService>(context, listen: false);
    final provider = Provider.of<ProviderInfor>(context);
    return Scaffold(
        appBar: AppBar(
          elevation: 15.0,
          centerTitle: true,
          backgroundColor: kPrimaryColor,
          title: Center(child: Text('Conversaciones', style: stiloText())),
          actions: [
            IconButton(
                icon: Icon(Icons.refresh, color: Colors.white),
                onPressed: () {
                  mostrarLoading(context, 'Cargando...');
                  provider.mostrarUltimoMensaje('', '');
                  pedirChat(socketService);
                })
          ],
        ),
        body: listarChats(provider));
  }

  pedirChat(socketService) {
    socketService.scoket.emit(chatActuales, {'id_niniera': idUser, 'type': 2});
  }

  clickChat(i) async {
    var perfil = {
      'first_name': _servicioChat[i]['personas'][0]['first_name'],
      'last_name': _servicioChat[i]['personas'][0]['last_name'],
      'photo': _servicioChat[i]['personas'][0].containsKey('photo')
          ? _servicioChat[i]['personas'][0]['photo']
          : null,
      'id_familia': _servicioChat[i]['identificacion'],
      'id_user': idUser
    };
    Navigator.of(context).pushNamed(ChatScreen.routeName, arguments: perfil);
  }

  listarChats(provider) {
    return totalChat > 0
        ? ListView.builder(
            itemCount: totalChat,
            itemBuilder: (context, index) => Column(
                  children: <Widget>[
                    Divider(
                      height: 5.0,
                    ),
                    ListTile(
                      leading: mostrarImagen(
                          _servicioChat[index]['personas'][0]['photo'],
                          context),
                      title: mostrarNombre(index),
                      subtitle: ultimoMensaje(index, provider),
                      onTap: () => {clickChat(index)},
                    ),
                  ],
                ))
        : Center(child: Text('Contrata el servicio!!!'));
  }

  Row mostrarNombre(int index) {
    String nombre = _servicioChat[index]['personas'][0]['first_name'];
    String apellido = _servicioChat[index]['personas'][0]['last_name'];
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Center(
          child: Text(
            nombre.toString() + " " + apellido.toString(),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Text(
          _servicioChat[index]['chat'].length > 0
              ? _servicioChat[index]['chat'][0]['hour'].toString()
              : '',
          style: TextStyle(fontSize: 14.0),
        ),
      ],
    );
  }

  Container ultimoMensaje(int index, provider) {
    return Container(
        padding: const EdgeInsets.only(top: 5.0),
        child: provider.ultimoMensaje.toString() == 'Foto'
            ? Row(
                children: [
                  Icon(Icons.camera_alt_rounded),
                  SizedBox(width: 5),
                  Text(provider.emisor.toString() == idUser
                      ? 'Tu: ' + provider.ultimoMensaje.toString()
                      : provider.ultimoMensaje.toString())
                ],
              )
            : Text(
                provider.emisor.toString() == idUser
                    ? 'Tu: ' + provider.ultimoMensaje.toString()
                    : provider.ultimoMensaje.toString(),
                style: TextStyle(color: Colors.grey, fontSize: 16.0),
              ));
  }
}
