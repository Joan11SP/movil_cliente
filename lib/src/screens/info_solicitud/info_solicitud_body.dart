import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/info_solicitud/info_controller.dart';
import 'package:movil_nanny/src/size_config.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:location/location.dart' as loc;

class InfoBody extends StatefulWidget {
  @override
  _InfoBodyState createState() => _InfoBodyState();
}

class _InfoBodyState extends State<InfoBody> {
  int start = 2;
  var infoPersonal;
  String marker = 'assets/images/ubi2.png';

  loc.Location location = loc.Location();
  String ubicacion = "assets/icons/Location point.svg";
  String usuario = "assets/icons/User.svg";
  String fecha = "assets/icons/calendario.svg";
  String reloj = "assets/icons/cronometro.svg";
  bool ubcacion = false;

  getPermiso() async {
    ubcacion = await validarPersimo(context);
    setState(() {});
  }

  @override
  void initState() {
    getPermiso();
    super.initState();
  }
  estiloDetalle(){
    return TextStyle(
      fontSize:16,fontFamily: 'Times New Roman',
      fontWeight: FontWeight.bold,
    );
  }
  @override
  Widget build(BuildContext context) {
    infoPersonal = ModalRoute.of(context).settings.arguments;
    print(infoPersonal);
    return SafeArea(
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(20),
              vertical: getProportionateScreenHeight(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    child: mostrarImagen(
                        infoPersonal['persona']['photo'], context),
                    height: getProportionateScreenHeight(150),
                    width: getProportionateScreenWidth(150),
                  ),
                  SizedBox(width: getProportionateScreenWidth(5)),
                  Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(
                              left: getProportionateScreenWidth(10)),
                          width: getProportionateScreenWidth(152),
                          child: Text(infoPersonal['persona']['first_name']
                                  .toString() +
                              " " +
                              infoPersonal['persona']['last_name'].toString(),style:estiloDetalle())),
                      SizedBox(height: getProportionateScreenHeight(15))
                    ],
                  )
                ],
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
              Text('Descripción',style:estiloDetalle()),
              SizedBox(height: getProportionateScreenHeight(15)),
              Container(
                child: Text(infoPersonal['solicitud']['descripcion'],style:TextStyle(fontSize:16)),
              ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Row(
                children: [
                  SvgPicture.asset(fecha,width: 35,color: kPrimaryColor,),
                  SizedBox(
                    width: getProportionateScreenWidth(15),
                  ),
                  Text('Fechas: ',style: estiloDetalle()),
                  SizedBox(width: getProportionateScreenWidth(10)),
                  infoPersonal['solicitud']['fecha_inicio'] ==
                          infoPersonal['solicitud']['fecha_fin']
                      ? Text(infoPersonal['solicitud']['fecha_inicio'])
                      : Text(infoPersonal['solicitud']['fecha_inicio'] +
                          " hasta " +
                          infoPersonal['solicitud']['fecha_fin'])
                ],
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
              Row(
                children: [
                  Expanded(                    
                    child: ListTile(
                      leading: SvgPicture.asset(reloj,width: 35,color: kPrimaryColor),
                      title: Text('Entrada',style: estiloDetalle(),),
                      subtitle:  Text(infoPersonal['solicitud']['hora_inicio'].toString()),
                    ),
                  ),
                  Expanded(
                      child: ListTile(
                        leading: SvgPicture.asset(reloj,width: 35,color: kPrimaryColor,),
                        title: Text('Salida',style: estiloDetalle()),
                        subtitle: Text(infoPersonal['solicitud']['hora_fin'].toString()),
                  )),
                ],
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
              Row(
                children: [
                  SvgPicture.asset(usuario,width: 30,color: kPrimaryColor,),
                  SizedBox(width: getProportionateScreenWidth(25)),
                  Text('Número de Niños' +
                  ' ' +
                  infoPersonal['solicitud']['numero_ninios'].toString(),style:estiloDetalle())
                ],
              ),
              
              SizedBox(height: getProportionateScreenHeight(15)),
              ListTile(
                trailing: SvgPicture.asset(ubicacion,width: 35,color: kPrimaryColor,),
                onTap: () => dibujarRuta(infoPersonal['ubicacion']['ubi_latitud'], infoPersonal['ubicacion']['ubi_longitud']),
                title:  Text(infoPersonal['ubicacion']['ubi_calles'],style: estiloDetalle()),
              ),

              SizedBox(height: getProportionateScreenHeight(15)),
              Container(
                child:Text(
                  'Horas de Servicio: ' + " " +infoPersonal['solicitud']['numero_horas'].toString(),
                  style:estiloDetalle()
                ),
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
              Container(
                child:Text(
                  'Referencia: ' + " " +infoPersonal['ubicacion']['ubi_referencia'],
                  style:estiloDetalle()
                ),
              ),
              SizedBox(height: getProportionateScreenHeight(15)),
            ],
          ),
        ),
      ),
    );
  }
  dibujarRuta(double latitud, double longitud) async {
    await MapLauncher.showDirections(
        directionsMode: DirectionsMode.driving,
        mapType: MapType.google,
        destination: Coords(latitud, longitud));
  }
}
