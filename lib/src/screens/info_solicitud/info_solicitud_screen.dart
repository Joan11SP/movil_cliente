import 'package:flutter/material.dart';
import 'package:movil_nanny/src/components/botones.dart';
import 'package:movil_nanny/src/constants.dart';
import 'package:movil_nanny/src/screens/info_solicitud/info_solicitud_body.dart';
import 'package:movil_nanny/src/size_config.dart';

class InfoSolicitudScreen extends StatelessWidget {
  static String routeName = '/info_solicitud';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 15.0,
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: Text('Detalle del Servicio',style: stiloText(),),
      ),
      body: InfoBody(),
    );
  }
}