import 'dart:convert';

String chatToJson(Chat data) => json.encode(data.toJson());
class Chat{

    String emisor;
    String receptor;
    String message;
    DateTime date;
    String hour;

    Chat({this.date,this.emisor,this.hour,this.message,this.receptor});

    // mapear un json
    factory Chat.desdeJson(Map<String, dynamic> json) => Chat(
        emisor: json["emisor"],
        receptor: json["receptor"],
        message:json['message'],
        date:json['date'],
        hour: json['hour']
      );

    //convertir a json
    Map<String, dynamic> toJson() => {
          "emisor": emisor,
          "receptor": receptor,
          "message": message,
          "hour": hour,
          "date": date
        };
}


List<ServicioChat> servicioChatFromJson(String str) => List<ServicioChat>.from(json.decode(str).map((x) => ServicioChat.fromJson(x)));

String servicioChatToJson(List<ServicioChat> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ServicioChat {
    ServicioChat({
        this.status,
        this.personas,
        this.identi,
        this.imagen,
    });

    int status;
    Personas personas;
    String identi;
    List<String> imagen;

    factory ServicioChat.fromJson(Map<String, dynamic> json) => ServicioChat(
        status: json["status"],
        personas: Personas.fromJson(json["personas"]),
        identi: json["identi"],
        imagen: List<String>.from(json["imagen"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "personas": personas.toJson(),
        "identi": identi,
        "imagen": List<dynamic>.from(imagen.map((x) => x)),
    };
}

class Personas {
    Personas({
        this.firstName,
        this.lastName,
    });

    String firstName;
    String lastName;

    factory Personas.fromJson(Map<String, dynamic> json) => Personas(
        firstName: json["first_name"],
        lastName: json["last_name"],
    );

    Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
    };
}
