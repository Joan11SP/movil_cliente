import 'package:flutter/widgets.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/screens/cambiar_contrasenia/screen_recuperar_contrasenia.dart';
import 'package:movil_nanny/src/screens/complete_profile/complete_profile_screen.dart';
import 'package:movil_nanny/src/screens/cuestionario/cues_archivos.dart';
import 'package:movil_nanny/src/screens/cuestionario/cuest_screen.dart';
import 'package:movil_nanny/src/screens/detalle_servicio/detalle_screen.dart';
import 'package:movil_nanny/src/screens/forgot_password/forgot_password_screen.dart';
import 'package:movil_nanny/src/screens/home/home_screen.dart';
import 'package:movil_nanny/src/screens/home/pendiente.dart';
import 'package:movil_nanny/src/screens/info_solicitud/info_solicitud_screen.dart';
import 'package:movil_nanny/src/screens/otp/otp_screen.dart';
import 'package:movil_nanny/src/screens/profile/notificacion.dart';
import 'package:movil_nanny/src/screens/profile/perfil.dart';
import 'package:movil_nanny/src/screens/profile/profile_screen.dart';
import 'package:movil_nanny/src/screens/sign_in/sign_in_screen.dart';
import 'package:movil_nanny/src/screens/solicitudes_servicio/solicitud_screen.dart';
import 'screens/sign_up/sign_up_screen.dart';
import 'screens/Chat/chat_bandeja.dart';
import 'screens/Chat/chat_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  registroPersona:(context) => CompleteProfileScreen(),
  recuperarContrasenia: (context) => RecuperarContraseniaScreen(),
  CuestionarioScreen.routeName: (context) => CuestionarioScreen(),
  SolicitudesScreen.routeName: (context) => SolicitudesScreen(),
  InfoSolicitudScreen.routeName: (context) => InfoSolicitudScreen(),
  BandejaChat.routeName:(context) =>BandejaChat(),
  ChatScreen.routeName:(context) => ChatScreen(),
  DetalleServicio.routeName: (context) => DetalleServicio(),
  PerfilScreen.routeName: (context) => PerfilScreen(),
  NotificacionScreen.routeName:(context) => NotificacionScreen(),
  PendienteScreen.routeName: (context) => PendienteScreen(),
  AddArchivos.routeName: (context) => AddArchivos()
};
