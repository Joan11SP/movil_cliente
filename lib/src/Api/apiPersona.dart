import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/Config/configEncrypt.dart';
import 'package:movil_nanny/src/Models/model_persona.dart';

var header = {
  "content-type": "application/json",
  'app':'app_niniera'
};
var headerNoJosn= {
  'app':'app_niniera'
};
var formdata = { "Content-Type":"multipart/form-data" };

loginPersonaApi(Login login){
  return http.post('$urlApi/login-persona',body: json.encode(login), headers: header);
}
nuevaPersonaApi(Persona persona){
  return http.post('$urlApi/crear-persona',body: personaToJson(persona), headers: header);
}
pedirOtpApi(String email,tipo){
  return http.post('$urlApi/generar-codigo-otp',body: {'email':email,'tipo_generador_otp':tipo},headers: headerNoJosn);
}
validarOtpApi(codigo, String id){
  return http.post('$urlApi/validar-codigo',body: {'codigo':codigo,'id':id},headers: headerNoJosn);
}
recuperarClaveApi(String passwordConfirmada, String id,String idCod){
  return http.post('$urlApi/cambiar-clave',body: {'password':passwordConfirmada,'id':id,'id_cod':idCod},headers: headerNoJosn);
}
crearCuestionarioApi(cuestionario){
  return http.post('$urlApi/crear-cuestionario-niniera', body: json.encode(cuestionario), headers: header);
}
buscarSolicitud(idNiniera){
  return http.post('$urlApi/buscar-solicitud',body: {'id_niniera':idNiniera},headers: headerNoJosn);
}
infoSolicitudApi(idNiniera,idfamilia){
  return http.post('$urlApi/mas-info-solicitud',body: 
  {'id_niniera':idNiniera, 'id_familia':idfamilia},
  headers: headerNoJosn);
}
crearPropuestaApi(ids){
  return http.post('$urlApi/crear-propuesta',body: json.encode(ids),
  headers: header);
}
verDetalleServicios(persona) {
  return http.post('$urlApi/detalle-servicios-niniera', body: {'id_niniera':persona}, headers: headerNoJosn);
}

detalleServicio(persona){
  return http.post('$urlApi/ver-servicios-realizados',body: {'id_niniera':persona}, headers: headerNoJosn);
}
buscarNotificacion(persona){
  return http.post('$urlApi/buscar-mis-notificaciones',body: {'id_persona':persona},headers: headerNoJosn);
}
obtenerCuestionario(persona){
  return http.post('$urlApi/cuestionario-ninera',body: {'id_niniera':persona},headers: headerNoJosn);
}
validarPendinte(persona){
  return http.post('$urlApi/actualizar-pendiente-ninera',body: {'id_niniera':persona},headers: headerNoJosn);
}

obtenerRecursoApi(url,body,header) async {
  var respuesta;
  try 
  {
    http.Response response = await http.post('$urlApi/$url',body: body,headers:header);    
    respuesta = response.body;
    
    if(respuestaEncriptada){
      respuesta = decryptAESCryptoJS(respuesta, secretKeyApi);
    }
    respuesta = json.decode(respuesta);

    return respuesta;
  } 
  on Exception catch (e) 
  {
    return respuesta = 0;
  }
  
}