
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movil_nanny/src/Config/config.dart';
import 'package:movil_nanny/src/components/default_button.dart';
import 'package:movil_nanny/src/size_config.dart';
import 'package:toast/toast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import '../constants.dart';

void mostrarLoading(BuildContext context, [String text]) {
  showDialog(
    //barrierColor: Colors.black.withOpacity(0.7),
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        content: Container(
          color: Colors.transparent,
          padding: EdgeInsets.all(10.0),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                backgroundColor: Colors.white,
                valueColor: colorLoading,
                strokeWidth: 1,
              ),
              SizedBox(
                height: 10.0,
              ),
              new Text(
                text,
                style: TextStyle(color: Colors.white, fontFamily: 'semibold'),
              ),
            ],
          ),
        ),
      );
    },
  );
}

mostrarMensaje(String msg, context, int duracion) {
  return Toast.show(msg, context, duration: duracion);
}

esconderLoading(context) {
  Navigator.of(context).pop();
}

mostrarImagen(img, context) {
  return img != null
      ? CircleAvatar(
          foregroundColor: Theme.of(context).primaryColor,
          backgroundImage: NetworkImage(img),
          onBackgroundImageError: (context, StackTrace stackTrace) {
              return AssetImage(imagenPrueba);
            },
          )
      : CircleAvatar(
          radius: 25,
          foregroundColor: Theme.of(context).primaryColor,
          backgroundColor: Colors.grey,
          backgroundImage: AssetImage(imagenPrueba));
}
subirImagenVarias(File imagen,context,String title,
double radius,Function cancelar,Function enviar) {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Text(title),
          actions: [
            FlatButton(
              onPressed:cancelar, 
              child: Text('Cancelar')),
            FlatButton(
              onPressed: enviar, 
              child: Text('Enviar'))
          ],
          content: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: CircleAvatar(
              maxRadius: radius,
              backgroundImage: FileImage(imagen),
            ),
            height: getProportionateScreenHeight(200),
            width: getProportionateScreenWidth(150),
          ),
        ));
  }
TextStyle stiloText([double textSize]){
  return TextStyle(
    color: Colors.white,
    fontSize: textSize == null ? 17.0 : textSize,
    fontWeight: FontWeight.w300,
  ); 
}
TextStyle estiloText([double textSize]){
  return TextStyle(
    color: Colors.black,
    fontSize: textSize == null ? 17.0 : textSize,
    fontWeight: FontWeight.w300,
  ); 
}
pintheme(){
  return PinTheme(
    inactiveColor: Colors.grey[400],
    activeColor: Colors.grey[400],
    disabledColor: Colors.grey[400],
    selectedColor: Colors.grey[400],
    shape: PinCodeFieldShape.box,
    borderRadius: BorderRadius.circular(5),
    fieldHeight: 50,
    fieldWidth: getProportionateScreenWidth(38),
  );
}

ingresarCodigoOtp(context, codigo,Function callback) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Ingrese el código enviado a tu correo' ,style: estiloText(),),
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            PinCodeTextField(
              showCursor: true,
              appContext: context,
              cursorColor: kSecondaryColor,
              animationType: AnimationType.fade,
              animationCurve: Curves.easeInBack,
              length: 6,
              obscureText: true,
              obscuringCharacter: '*',
              autoFocus: false,
              dialogConfig: DialogConfig(dialogTitle: 'OTP'),
              pastedTextStyle: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
              pinTheme: pintheme(),
              enableActiveFill: false,
              keyboardType: TextInputType.number,
              onCompleted: (v) {                
                  codigo = v;
              },
              onChanged: (value) {
                codigo = value;
              },
            ),
            DefaultButton(
              text: 'Validar',
              color: kSecondaryColor,
              press: () async {
                if(codigo.length == 6){
                  return callback(codigo);
                }
              },
            )
          ],
        ),
      );
    },
  );
}